{
    "id": "8076cf0b-b033-4c23-9134-fb21a2e4bf47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "44233626-24c2-417b-8980-a9467cb0c45e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8076cf0b-b033-4c23-9134-fb21a2e4bf47"
        },
        {
            "id": "751779e7-f65f-4a3c-8044-194f07f95573",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8076cf0b-b033-4c23-9134-fb21a2e4bf47"
        },
        {
            "id": "1c62e86a-a508-4c67-8b98-5dda0fb001aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "8076cf0b-b033-4c23-9134-fb21a2e4bf47"
        },
        {
            "id": "afda24ec-82e3-4522-bce8-960f30c416f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 2,
            "m_owner": "8076cf0b-b033-4c23-9134-fb21a2e4bf47"
        }
    ],
    "maskSpriteId": "c0046a67-bdaf-4381-887d-29840846b695",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0046a67-bdaf-4381-887d-29840846b695",
    "visible": true
}