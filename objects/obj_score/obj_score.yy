{
    "id": "37e5de9f-e2ab-4ebc-af62-e62376e6a393",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_score",
    "eventList": [
        {
            "id": "e7c2a6d9-5966-41ce-bd48-e9a7b3359273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37e5de9f-e2ab-4ebc-af62-e62376e6a393"
        },
        {
            "id": "9985b615-30e1-414e-9180-ec0bdc005265",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "37e5de9f-e2ab-4ebc-af62-e62376e6a393"
        },
        {
            "id": "1638e324-ab09-4286-a480-c8957a87eb27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37e5de9f-e2ab-4ebc-af62-e62376e6a393"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}