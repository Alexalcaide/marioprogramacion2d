{
    "id": "80d816c6-b7e2-4ce5-80d8-505211e62565",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "moneda1",
    "eventList": [
        {
            "id": "2fceff7d-f4f3-4566-824a-0c458e5bb8e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80d816c6-b7e2-4ce5-80d8-505211e62565"
        },
        {
            "id": "488d5ac7-e035-41e8-a8c6-e03fa13fa1a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "80d816c6-b7e2-4ce5-80d8-505211e62565"
        },
        {
            "id": "14348a51-c1c2-4d08-b2f4-9c8ab8d3cb47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "80d816c6-b7e2-4ce5-80d8-505211e62565"
        },
        {
            "id": "f55ba354-4251-4c2f-a158-dbffda73a52f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "80d816c6-b7e2-4ce5-80d8-505211e62565"
        },
        {
            "id": "8e07ea96-d1df-4a66-9c3b-9c79975cafe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "336e5893-a547-47dd-83a5-e275352535b3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "80d816c6-b7e2-4ce5-80d8-505211e62565"
        }
    ],
    "maskSpriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
    "visible": true
}