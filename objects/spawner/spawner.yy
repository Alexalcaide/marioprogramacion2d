{
    "id": "dc6e15d6-297e-4a33-8620-f1baa237b63e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "spawner",
    "eventList": [
        {
            "id": "7f80ee7d-20d1-44c9-a7c6-8a68d607c267",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc6e15d6-297e-4a33-8620-f1baa237b63e"
        },
        {
            "id": "ac6c64ae-ae29-4505-a5c2-fd3053a40876",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dc6e15d6-297e-4a33-8620-f1baa237b63e"
        },
        {
            "id": "cb96e130-595c-4934-9b22-f2b528c55c69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "dc6e15d6-297e-4a33-8620-f1baa237b63e"
        },
        {
            "id": "1cb8bc0e-4b7e-4c66-9220-afcb4ae7a30b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc6e15d6-297e-4a33-8620-f1baa237b63e"
        },
        {
            "id": "895b6247-2456-41b4-abe5-c394b3f10027",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "dc6e15d6-297e-4a33-8620-f1baa237b63e"
        },
        {
            "id": "0fdbca73-5212-4f00-a677-ae8689c3c5d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "dc6e15d6-297e-4a33-8620-f1baa237b63e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}