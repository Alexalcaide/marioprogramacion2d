{
    "id": "7170d4f6-bc77-4d24-8ca6-e43967151311",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Enemy",
    "eventList": [
        {
            "id": "fc6ef0b1-89cc-454b-ba7d-c8b86a2bd535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7170d4f6-bc77-4d24-8ca6-e43967151311"
        },
        {
            "id": "969469f3-0db9-4911-a42d-04d49dc1f13f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7170d4f6-bc77-4d24-8ca6-e43967151311"
        },
        {
            "id": "5ec6a8a5-3680-44f6-bbae-d51579a94f03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7170d4f6-bc77-4d24-8ca6-e43967151311"
        },
        {
            "id": "c2d41b4d-0a45-40db-a999-cabc3511f1f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "336e5893-a547-47dd-83a5-e275352535b3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7170d4f6-bc77-4d24-8ca6-e43967151311"
        },
        {
            "id": "215a5a98-cb12-4cf3-8143-66c71cffcb93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7170d4f6-bc77-4d24-8ca6-e43967151311"
        }
    ],
    "maskSpriteId": "ec41f059-c50a-4268-89f7-3d78842b99a5",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec41f059-c50a-4268-89f7-3d78842b99a5",
    "visible": true
}