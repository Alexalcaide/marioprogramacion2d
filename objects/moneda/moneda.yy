{
    "id": "436e343b-507d-44bc-95a5-7d84833646a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "moneda",
    "eventList": [
        {
            "id": "478b55fc-e7a3-49e5-9544-7d95a1e44c45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "436e343b-507d-44bc-95a5-7d84833646a1"
        },
        {
            "id": "f3769bdf-bcd8-4351-915e-6aa715bfbcb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "436e343b-507d-44bc-95a5-7d84833646a1"
        },
        {
            "id": "58956fee-0b53-48a9-9292-ac0e020f261d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "436e343b-507d-44bc-95a5-7d84833646a1"
        },
        {
            "id": "5fdbdbe4-c984-4e9f-b7f0-480cffb48371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "436e343b-507d-44bc-95a5-7d84833646a1"
        },
        {
            "id": "32b5ee08-a28f-439b-99fd-6b478674ff41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "336e5893-a547-47dd-83a5-e275352535b3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "436e343b-507d-44bc-95a5-7d84833646a1"
        }
    ],
    "maskSpriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
    "visible": true
}