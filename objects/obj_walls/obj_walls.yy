{
    "id": "07f88870-1503-426e-bc9f-b5390dc36ef3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_walls",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5bb7abf9-8180-4471-b9f6-2bc60d87d5c1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "88eebdf7-e935-42f1-9c3b-eb23fc21fb84",
    "visible": true
}