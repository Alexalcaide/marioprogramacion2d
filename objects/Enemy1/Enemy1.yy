{
    "id": "2591ab9b-4564-4883-b4f4-21a1387470db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Enemy1",
    "eventList": [
        {
            "id": "24c2c35c-edf5-4ba8-b3ab-ec0813c3d54a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2591ab9b-4564-4883-b4f4-21a1387470db"
        },
        {
            "id": "6e2320bb-e7ed-4fcb-a38c-c8f353361fe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2591ab9b-4564-4883-b4f4-21a1387470db"
        },
        {
            "id": "c8cc9534-02e8-4457-b296-429685bc6aff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "2591ab9b-4564-4883-b4f4-21a1387470db"
        },
        {
            "id": "97ec8ae9-5179-4062-8029-790c22a7501f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "336e5893-a547-47dd-83a5-e275352535b3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2591ab9b-4564-4883-b4f4-21a1387470db"
        },
        {
            "id": "bdee4bb7-2ad8-4a17-ae88-ef8b976d75cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2591ab9b-4564-4883-b4f4-21a1387470db"
        }
    ],
    "maskSpriteId": "ec41f059-c50a-4268-89f7-3d78842b99a5",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec41f059-c50a-4268-89f7-3d78842b99a5",
    "visible": true
}