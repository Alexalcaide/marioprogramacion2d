{
    "id": "8dbe17b3-5438-49b2-b3e5-8b1f99a0062e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_layer3",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 265,
    "bbox_left": 50,
    "bbox_right": 1474,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53aaed5e-9a7f-4949-b39e-296e15963a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dbe17b3-5438-49b2-b3e5-8b1f99a0062e",
            "compositeImage": {
                "id": "eca9faa9-3421-41a1-a6e4-0af9af44eefa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53aaed5e-9a7f-4949-b39e-296e15963a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea4efbb-a9b0-43c3-8ecf-df67b385ce90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53aaed5e-9a7f-4949-b39e-296e15963a26",
                    "LayerId": "497c0582-5e24-4dd4-9af8-c40851d1eaec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "497c0582-5e24-4dd4-9af8-c40851d1eaec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dbe17b3-5438-49b2-b3e5-8b1f99a0062e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1600,
    "xorig": 0,
    "yorig": 0
}