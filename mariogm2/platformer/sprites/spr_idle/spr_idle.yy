{
    "id": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 0,
    "bbox_right": 67,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5545d48d-0e40-4e3c-8c60-420fc471ef8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "e381e976-c2d8-4da8-bdfb-850a45ffe6c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5545d48d-0e40-4e3c-8c60-420fc471ef8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1367b10-04c6-4221-b5a6-dd94b217ce2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5545d48d-0e40-4e3c-8c60-420fc471ef8f",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "40f6acac-e9e8-479f-854f-ea19a8e06351",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "a8583c88-2c52-407b-9597-88af7b40260d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40f6acac-e9e8-479f-854f-ea19a8e06351",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0269ab9-23fe-4b6f-a8bf-d207f4eb6b6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40f6acac-e9e8-479f-854f-ea19a8e06351",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "ef85f34a-1b68-46a2-91e9-ba0c775409c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "6d6d0fcd-140b-479b-97f2-ffadb49f1db9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef85f34a-1b68-46a2-91e9-ba0c775409c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8224341-db97-49fb-887f-ab80ab640dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef85f34a-1b68-46a2-91e9-ba0c775409c8",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "fa7ab06d-28ea-420a-b598-8b64fea3d0fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "26aec48c-eb6d-40be-b31a-70ec05d6ecc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa7ab06d-28ea-420a-b598-8b64fea3d0fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e3b3ab5-bf84-4644-a4fb-166f0fd7ea68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa7ab06d-28ea-420a-b598-8b64fea3d0fc",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "7fd0a91c-2798-49f8-a71e-5f1c1d6ec28b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "9a9645f7-3525-464b-a1e4-ceadd1477145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fd0a91c-2798-49f8-a71e-5f1c1d6ec28b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2add816-087a-48d1-92a2-16f437de614b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fd0a91c-2798-49f8-a71e-5f1c1d6ec28b",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "162d2f8d-4b95-4cbe-82b1-9587a58fce35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "36384ea0-43f8-4c8d-97e8-e30b644f76a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "162d2f8d-4b95-4cbe-82b1-9587a58fce35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "973de481-b034-4719-aa5d-441152d657a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "162d2f8d-4b95-4cbe-82b1-9587a58fce35",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "2cbfbe64-4f66-425f-ae3c-fa8984669345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "b4ed2ce6-b13f-4cc7-ade2-6b8adfe75ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cbfbe64-4f66-425f-ae3c-fa8984669345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1b6df16-0321-4001-a3c6-01440c1c2a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cbfbe64-4f66-425f-ae3c-fa8984669345",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        },
        {
            "id": "89c01094-0508-4c16-a451-87ff83f079c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "compositeImage": {
                "id": "447c6759-e61c-4978-bfd0-7872b9847fae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89c01094-0508-4c16-a451-87ff83f079c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74bf03ba-0ce2-41cc-8c5f-83424fdbe22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89c01094-0508-4c16-a451-87ff83f079c5",
                    "LayerId": "a8860d32-ee66-4985-abc7-e9c664426bfa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "a8860d32-ee66-4985-abc7-e9c664426bfa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2889c4e5-f8a4-4bf8-b989-0ac520431f0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 42
}