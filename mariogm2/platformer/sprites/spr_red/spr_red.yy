{
    "id": "5555a008-85ec-4bbe-848d-cdbb6a2b7164",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b82dcbc9-d768-48d5-b9db-37b48a7c2411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5555a008-85ec-4bbe-848d-cdbb6a2b7164",
            "compositeImage": {
                "id": "166cef9c-0182-4d4f-a07b-71cee3a3ecf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b82dcbc9-d768-48d5-b9db-37b48a7c2411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57867a80-905b-46df-b77b-82628c43956a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b82dcbc9-d768-48d5-b9db-37b48a7c2411",
                    "LayerId": "06ef900e-99e7-46dc-a1a7-64618d45b7be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "06ef900e-99e7-46dc-a1a7-64618d45b7be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5555a008-85ec-4bbe-848d-cdbb6a2b7164",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}