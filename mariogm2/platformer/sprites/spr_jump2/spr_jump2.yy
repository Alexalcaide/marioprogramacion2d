{
    "id": "bbca9d71-5097-42ad-8fe8-c8a875d74aab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 4,
    "bbox_right": 74,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed447e3f-5a9c-4303-8c3a-5a030db37d13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbca9d71-5097-42ad-8fe8-c8a875d74aab",
            "compositeImage": {
                "id": "22191a34-da32-46a3-8cd9-eed9d5cd9a25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed447e3f-5a9c-4303-8c3a-5a030db37d13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c762886-5025-4802-ab03-47029abc620a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed447e3f-5a9c-4303-8c3a-5a030db37d13",
                    "LayerId": "bec88ff2-919a-4d36-a3e6-53706072c5b6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "bec88ff2-919a-4d36-a3e6-53706072c5b6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbca9d71-5097-42ad-8fe8-c8a875d74aab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}