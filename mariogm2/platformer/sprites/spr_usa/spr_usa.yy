{
    "id": "6d43a0d1-1240-4ba9-9029-2e0ff157dc35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_usa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 0,
    "bbox_right": 77,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c54393e-5963-4c7f-bcc5-2ee7ad93664c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d43a0d1-1240-4ba9-9029-2e0ff157dc35",
            "compositeImage": {
                "id": "46f2c4a9-c585-4457-ab75-cd2cd9cde77d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c54393e-5963-4c7f-bcc5-2ee7ad93664c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "833472e8-a5d3-45fa-8a53-3c4cbdfdea4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c54393e-5963-4c7f-bcc5-2ee7ad93664c",
                    "LayerId": "c521455a-a27a-446a-b52c-08033c7297d4"
                }
            ]
        },
        {
            "id": "2c124e6e-d17d-40ac-bef5-486d1e801f87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d43a0d1-1240-4ba9-9029-2e0ff157dc35",
            "compositeImage": {
                "id": "561d20aa-2747-4d6c-8b01-46ba51d0ddce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c124e6e-d17d-40ac-bef5-486d1e801f87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925e6596-78a4-4d83-b3fa-7b1503d2beca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c124e6e-d17d-40ac-bef5-486d1e801f87",
                    "LayerId": "c521455a-a27a-446a-b52c-08033c7297d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "c521455a-a27a-446a-b52c-08033c7297d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d43a0d1-1240-4ba9-9029-2e0ff157dc35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}