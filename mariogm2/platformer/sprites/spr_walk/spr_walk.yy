{
    "id": "66a8af76-ab16-477a-ac56-06a221b60143",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 0,
    "bbox_right": 67,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc7b80d7-c2f1-493a-9d49-67a7b04e34dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a8af76-ab16-477a-ac56-06a221b60143",
            "compositeImage": {
                "id": "7a08a246-2e95-4e8f-b813-3888bf04f140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7b80d7-c2f1-493a-9d49-67a7b04e34dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f6d295-3ea0-4e46-a7d1-64aaee0d3793",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7b80d7-c2f1-493a-9d49-67a7b04e34dc",
                    "LayerId": "a52d37d1-3e21-4a41-97ae-0019a97b35a4"
                }
            ]
        },
        {
            "id": "2f349550-d71f-4fc7-93de-e0adfc6af961",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a8af76-ab16-477a-ac56-06a221b60143",
            "compositeImage": {
                "id": "a9b7a13e-5479-44e8-b955-0a6e468429b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f349550-d71f-4fc7-93de-e0adfc6af961",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1a93f02-07a5-49e2-946b-5fe636e2392b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f349550-d71f-4fc7-93de-e0adfc6af961",
                    "LayerId": "a52d37d1-3e21-4a41-97ae-0019a97b35a4"
                }
            ]
        },
        {
            "id": "a7fc7525-fd55-4d07-83df-9a6a8a19f226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a8af76-ab16-477a-ac56-06a221b60143",
            "compositeImage": {
                "id": "1fc978c2-95e5-4a87-a82d-9de268324bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fc7525-fd55-4d07-83df-9a6a8a19f226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6215d054-c947-4225-b4ac-f440300e39ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fc7525-fd55-4d07-83df-9a6a8a19f226",
                    "LayerId": "a52d37d1-3e21-4a41-97ae-0019a97b35a4"
                }
            ]
        },
        {
            "id": "cd18c245-884a-4e05-8037-cbfe1715ad02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66a8af76-ab16-477a-ac56-06a221b60143",
            "compositeImage": {
                "id": "d1177cdf-40fd-4d87-8ca2-044394737fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd18c245-884a-4e05-8037-cbfe1715ad02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ad9264-b60e-4cef-93cd-879da8763778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd18c245-884a-4e05-8037-cbfe1715ad02",
                    "LayerId": "a52d37d1-3e21-4a41-97ae-0019a97b35a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "a52d37d1-3e21-4a41-97ae-0019a97b35a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66a8af76-ab16-477a-ac56-06a221b60143",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 42
}