{
    "id": "0d2f35be-cadc-483b-9e35-6be573fd733d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 9,
    "bbox_right": 71,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b715e3a9-c21b-4042-8f10-a25e8e63b597",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d2f35be-cadc-483b-9e35-6be573fd733d",
            "compositeImage": {
                "id": "da8b056a-969b-4da3-b9d1-4e10c5e1b8ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b715e3a9-c21b-4042-8f10-a25e8e63b597",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "934c704a-7efd-405c-b8ba-33561cd029d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b715e3a9-c21b-4042-8f10-a25e8e63b597",
                    "LayerId": "ef3459bc-a231-4906-965a-b5517055cbbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "ef3459bc-a231-4906-965a-b5517055cbbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d2f35be-cadc-483b-9e35-6be573fd733d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}