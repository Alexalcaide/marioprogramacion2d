{
    "id": "2a6147c9-8fe6-41b3-aa48-6084ee18774c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 109,
    "bbox_left": 3,
    "bbox_right": 74,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43828bc2-96d8-4648-9a53-044672170a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a6147c9-8fe6-41b3-aa48-6084ee18774c",
            "compositeImage": {
                "id": "350d4da2-34b1-4bda-a64c-83986ca48b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43828bc2-96d8-4648-9a53-044672170a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6de3d4-9334-46a7-8d58-70d6db6eafa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43828bc2-96d8-4648-9a53-044672170a3d",
                    "LayerId": "0f9d3024-f716-4d10-aac0-c13b0b3b8cf7"
                }
            ]
        },
        {
            "id": "cdd5ba91-df13-4558-b2d3-d1c58c31ee41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a6147c9-8fe6-41b3-aa48-6084ee18774c",
            "compositeImage": {
                "id": "0515b038-56cb-4032-b333-f2bafbb07e7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd5ba91-df13-4558-b2d3-d1c58c31ee41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d12195dc-9496-429e-a244-de9294bf0787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd5ba91-df13-4558-b2d3-d1c58c31ee41",
                    "LayerId": "0f9d3024-f716-4d10-aac0-c13b0b3b8cf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 110,
    "layers": [
        {
            "id": "0f9d3024-f716-4d10-aac0-c13b0b3b8cf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a6147c9-8fe6-41b3-aa48-6084ee18774c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}