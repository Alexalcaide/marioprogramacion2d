{
    "id": "dd04160a-74f7-4668-9630-4c12a45c6d9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_layer2",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 639,
    "bbox_left": 0,
    "bbox_right": 1599,
    "bbox_top": 410,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "926d7e37-a278-468b-ac2f-8a6940ff0d70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd04160a-74f7-4668-9630-4c12a45c6d9a",
            "compositeImage": {
                "id": "b81ec60a-56d1-455d-8cb0-230f7069a94e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926d7e37-a278-468b-ac2f-8a6940ff0d70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07d2eef5-f102-49f4-aea4-0e638f1fa4ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926d7e37-a278-468b-ac2f-8a6940ff0d70",
                    "LayerId": "45806a55-b52f-4e25-8136-163ea4942d45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "45806a55-b52f-4e25-8136-163ea4942d45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd04160a-74f7-4668-9630-4c12a45c6d9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1600,
    "xorig": 0,
    "yorig": 0
}