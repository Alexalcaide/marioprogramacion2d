{
    "id": "8f994121-7ebf-43b8-8821-d1a1a83f8aa2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fc7c518-1939-41c7-beee-4b904eca2059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f994121-7ebf-43b8-8821-d1a1a83f8aa2",
            "compositeImage": {
                "id": "36e12352-8ce6-4656-aed2-013ebbd477ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc7c518-1939-41c7-beee-4b904eca2059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a596e098-41c2-4771-a155-fe92e451d226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc7c518-1939-41c7-beee-4b904eca2059",
                    "LayerId": "f813917e-4bfc-4ba5-93b5-884169caf134"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f813917e-4bfc-4ba5-93b5-884169caf134",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f994121-7ebf-43b8-8821-d1a1a83f8aa2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}