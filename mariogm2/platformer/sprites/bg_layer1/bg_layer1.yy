{
    "id": "9b96ee50-32de-4407-bcc6-fb469cb4cd1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_layer1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 639,
    "bbox_left": 25,
    "bbox_right": 1243,
    "bbox_top": 266,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ccee9f6-1f67-4be1-8070-2efc04b9a959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b96ee50-32de-4407-bcc6-fb469cb4cd1d",
            "compositeImage": {
                "id": "6e7d08c4-71f8-47b5-b09c-042f4f080029",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ccee9f6-1f67-4be1-8070-2efc04b9a959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "897d5f2d-3cab-4065-a6b7-48a276a6ed63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ccee9f6-1f67-4be1-8070-2efc04b9a959",
                    "LayerId": "b5236af1-ac65-4503-91de-370646dc327f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "b5236af1-ac65-4503-91de-370646dc327f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b96ee50-32de-4407-bcc6-fb469cb4cd1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1600,
    "xorig": 0,
    "yorig": 0
}