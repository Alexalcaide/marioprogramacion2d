{
    "id": "880e7054-38a6-4aaa-9489-99039c7f9bdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 0,
    "bbox_right": 67,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd2ff28b-7fa8-4f8c-aea0-be83b2cbb8dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "880e7054-38a6-4aaa-9489-99039c7f9bdd",
            "compositeImage": {
                "id": "7874bb57-4bce-47c5-b573-ac8561477aff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2ff28b-7fa8-4f8c-aea0-be83b2cbb8dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ae40010-d7e8-4d5b-9865-4094b78dba5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2ff28b-7fa8-4f8c-aea0-be83b2cbb8dc",
                    "LayerId": "3b0a8219-10f9-48a4-895b-4b55d84e42c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 84,
    "layers": [
        {
            "id": "3b0a8219-10f9-48a4-895b-4b55d84e42c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "880e7054-38a6-4aaa-9489-99039c7f9bdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 34,
    "yorig": 42
}