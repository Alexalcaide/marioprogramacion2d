{
    "id": "9c6ad026-8d4d-4945-8f42-7ebc48c067f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f4ad7b7-4899-4e04-b641-b6800c01d66b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c6ad026-8d4d-4945-8f42-7ebc48c067f5",
            "compositeImage": {
                "id": "50666d11-7bd6-4040-8b92-203f50d1bef1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f4ad7b7-4899-4e04-b641-b6800c01d66b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b2ca3a6-108b-484e-9157-16cf541adbdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f4ad7b7-4899-4e04-b641-b6800c01d66b",
                    "LayerId": "0004f3de-1026-4ce3-b473-8fd238c9a449"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0004f3de-1026-4ce3-b473-8fd238c9a449",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c6ad026-8d4d-4945-8f42-7ebc48c067f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}