{
    "id": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_moneda_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "747db2c2-2233-4ce0-a563-2b1f41c380c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
            "compositeImage": {
                "id": "4b781dc7-3e19-4331-8aeb-ef7699eff93a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747db2c2-2233-4ce0-a563-2b1f41c380c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60c39554-9538-422a-907e-88e506166307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747db2c2-2233-4ce0-a563-2b1f41c380c8",
                    "LayerId": "2c57f171-df76-4ca0-810e-a7c2900afafa"
                }
            ]
        },
        {
            "id": "e792fbad-d8af-4ce2-b180-c3e8d5c38ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
            "compositeImage": {
                "id": "bfbd2356-0740-49be-9ba0-bf826622f360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e792fbad-d8af-4ce2-b180-c3e8d5c38ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6286472-4189-4e55-9dee-7e9f83698310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e792fbad-d8af-4ce2-b180-c3e8d5c38ca6",
                    "LayerId": "2c57f171-df76-4ca0-810e-a7c2900afafa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "2c57f171-df76-4ca0-810e-a7c2900afafa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe6d88a9-2ffc-4945-a0b0-4c0236824680",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}