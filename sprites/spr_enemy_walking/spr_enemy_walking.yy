{
    "id": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_walking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aab81b30-129d-41ec-8f91-cb47c091a511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
            "compositeImage": {
                "id": "9f77a428-4883-410d-b51b-d66b33041452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab81b30-129d-41ec-8f91-cb47c091a511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e04039-b102-44bb-b47b-7f161ce7da71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab81b30-129d-41ec-8f91-cb47c091a511",
                    "LayerId": "56cf68ff-9dea-4eb2-8c69-c2d434d77e3f"
                }
            ]
        },
        {
            "id": "6f340f29-cc5d-4e01-b0b3-e01a7f17cc70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
            "compositeImage": {
                "id": "cd177e36-5d20-43f4-aa72-83dd0d0936a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f340f29-cc5d-4e01-b0b3-e01a7f17cc70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67f01836-76e6-42e2-977b-4d358f789aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f340f29-cc5d-4e01-b0b3-e01a7f17cc70",
                    "LayerId": "56cf68ff-9dea-4eb2-8c69-c2d434d77e3f"
                }
            ]
        },
        {
            "id": "25f56a9a-92e7-448b-b337-df8479a1491f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
            "compositeImage": {
                "id": "d8ce42cf-c7d6-417b-9901-0ac9408154e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25f56a9a-92e7-448b-b337-df8479a1491f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dadf8de-caee-4ee1-a8a8-85b85769dd5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25f56a9a-92e7-448b-b337-df8479a1491f",
                    "LayerId": "56cf68ff-9dea-4eb2-8c69-c2d434d77e3f"
                }
            ]
        },
        {
            "id": "f56cd036-b7cf-407e-83e0-c2a4b4395173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
            "compositeImage": {
                "id": "b12daea3-00f5-4173-a7e9-c2b95bdd48b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56cd036-b7cf-407e-83e0-c2a4b4395173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a37805-422c-4b78-84d9-6365d0f1c999",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56cd036-b7cf-407e-83e0-c2a4b4395173",
                    "LayerId": "56cf68ff-9dea-4eb2-8c69-c2d434d77e3f"
                }
            ]
        },
        {
            "id": "ae38b8a5-8279-4056-a711-0cfca166a5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
            "compositeImage": {
                "id": "87b82f7c-b9ee-483e-bb88-a8caa99a43bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae38b8a5-8279-4056-a711-0cfca166a5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8b75c7-f0a6-4f08-844a-162864915ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae38b8a5-8279-4056-a711-0cfca166a5b0",
                    "LayerId": "56cf68ff-9dea-4eb2-8c69-c2d434d77e3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "56cf68ff-9dea-4eb2-8c69-c2d434d77e3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fa440a2-3ea6-4bd2-b0cf-4e5dc68ed435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}