{
    "id": "cfd8df4b-f140-4fa9-bb50-7ad2510ad067",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 21,
    "bbox_right": 43,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e95e5dda-5df7-4ec6-a82b-329de4babd11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cfd8df4b-f140-4fa9-bb50-7ad2510ad067",
            "compositeImage": {
                "id": "60e194c7-b3b3-4900-b415-651967fd25b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95e5dda-5df7-4ec6-a82b-329de4babd11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88643f26-31ba-4cf6-b4cf-b95b93794f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95e5dda-5df7-4ec6-a82b-329de4babd11",
                    "LayerId": "81694e97-4dc9-499d-b1d2-e7f0fb3e7ea4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "81694e97-4dc9-499d-b1d2-e7f0fb3e7ea4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cfd8df4b-f140-4fa9-bb50-7ad2510ad067",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}