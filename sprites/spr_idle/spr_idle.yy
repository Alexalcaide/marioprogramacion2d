{
    "id": "c0046a67-bdaf-4381-887d-29840846b695",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f342b5a5-1b18-4c47-9793-d2b69041059e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0046a67-bdaf-4381-887d-29840846b695",
            "compositeImage": {
                "id": "952f458a-f9ee-42c6-bfe0-00c5f14abcd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f342b5a5-1b18-4c47-9793-d2b69041059e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "859472f3-f541-4d80-ace6-5687eac5b09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f342b5a5-1b18-4c47-9793-d2b69041059e",
                    "LayerId": "35ed6b1a-dbdc-4010-bb56-b1e650f85e07"
                }
            ]
        },
        {
            "id": "7d0c81fc-3cfa-4051-80d5-b7156c528485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0046a67-bdaf-4381-887d-29840846b695",
            "compositeImage": {
                "id": "bbb7ae05-3031-48ad-af45-49227f525907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d0c81fc-3cfa-4051-80d5-b7156c528485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "112a709a-8645-448c-8b04-ab05fcf7cead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d0c81fc-3cfa-4051-80d5-b7156c528485",
                    "LayerId": "35ed6b1a-dbdc-4010-bb56-b1e650f85e07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "35ed6b1a-dbdc-4010-bb56-b1e650f85e07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0046a67-bdaf-4381-887d-29840846b695",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 20
}