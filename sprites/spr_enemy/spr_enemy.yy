{
    "id": "ec41f059-c50a-4268-89f7-3d78842b99a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c40d37eb-c540-489e-8e46-d4475538a8f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec41f059-c50a-4268-89f7-3d78842b99a5",
            "compositeImage": {
                "id": "3376f2b0-30c1-4318-bdc1-cd70b134ca88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40d37eb-c540-489e-8e46-d4475538a8f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea8f7ce-1556-486d-a841-2a5d40e4529b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40d37eb-c540-489e-8e46-d4475538a8f8",
                    "LayerId": "c90f1d75-00aa-4fa8-ad1c-5de40c7c3a31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c90f1d75-00aa-4fa8-ad1c-5de40c7c3a31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec41f059-c50a-4268-89f7-3d78842b99a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}