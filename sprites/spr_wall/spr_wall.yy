{
    "id": "88eebdf7-e935-42f1-9c3b-eb23fc21fb84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 253,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9998128-56e9-489b-85f6-bdbb776373e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88eebdf7-e935-42f1-9c3b-eb23fc21fb84",
            "compositeImage": {
                "id": "0e034ec7-3bf3-4693-abee-25254a79adc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9998128-56e9-489b-85f6-bdbb776373e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c8c275f-4ca5-44e9-96b8-8a33d0fe458f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9998128-56e9-489b-85f6-bdbb776373e4",
                    "LayerId": "7f8c355d-ff8f-47fb-8604-7f8797df8a26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7f8c355d-ff8f-47fb-8604-7f8797df8a26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88eebdf7-e935-42f1-9c3b-eb23fc21fb84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 254,
    "xorig": 127,
    "yorig": 8
}