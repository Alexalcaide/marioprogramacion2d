{
    "id": "2904f51c-e566-4e7d-9fbb-4c16d217b47e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 253,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d68abec7-9668-4c82-98b2-0d1d276a0594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2904f51c-e566-4e7d-9fbb-4c16d217b47e",
            "compositeImage": {
                "id": "ac38a1c6-3344-441a-9a33-041444d272d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d68abec7-9668-4c82-98b2-0d1d276a0594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9c8f53f-ffdf-43b8-a024-8763ee736a2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d68abec7-9668-4c82-98b2-0d1d276a0594",
                    "LayerId": "f59697c6-1c09-444b-8ef7-bfaa18a3179d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "f59697c6-1c09-444b-8ef7-bfaa18a3179d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2904f51c-e566-4e7d-9fbb-4c16d217b47e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 254,
    "xorig": 0,
    "yorig": 0
}