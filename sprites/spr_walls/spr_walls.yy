{
    "id": "31218401-1795-4890-a9aa-9b614453a233",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2865babc-c3cb-4f30-a272-4eab9421a075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31218401-1795-4890-a9aa-9b614453a233",
            "compositeImage": {
                "id": "63e1e521-4a12-47a0-96d5-39ea660671eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2865babc-c3cb-4f30-a272-4eab9421a075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9253dc-197a-426f-8a70-af3733447311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2865babc-c3cb-4f30-a272-4eab9421a075",
                    "LayerId": "55a7a16a-7c45-4ec4-8168-ec65b6ed3e86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "55a7a16a-7c45-4ec4-8168-ec65b6ed3e86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31218401-1795-4890-a9aa-9b614453a233",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 3
}