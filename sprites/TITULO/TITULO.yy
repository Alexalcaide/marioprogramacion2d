{
    "id": "16e7f381-5342-4ff0-b4b4-2247ee477268",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "TITULO",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 100,
    "bbox_left": 7,
    "bbox_right": 102,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ad5bd16-3900-41a9-b770-e16a9e09cf09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "16e7f381-5342-4ff0-b4b4-2247ee477268",
            "compositeImage": {
                "id": "5bc2f371-6ffe-4d99-8dcc-ae55598d6302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ad5bd16-3900-41a9-b770-e16a9e09cf09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b28ec67-4399-4723-bdf3-6c3374842596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ad5bd16-3900-41a9-b770-e16a9e09cf09",
                    "LayerId": "adee086c-508c-41e5-a0e6-049a9711261f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "adee086c-508c-41e5-a0e6-049a9711261f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "16e7f381-5342-4ff0-b4b4-2247ee477268",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}