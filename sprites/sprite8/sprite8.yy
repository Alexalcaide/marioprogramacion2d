{
    "id": "a936ed9f-4cb9-4fce-8704-0b6eed565176",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf7e5df2-365d-427a-af1b-ada3c3ac9add",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a936ed9f-4cb9-4fce-8704-0b6eed565176",
            "compositeImage": {
                "id": "741b9f7c-cfe6-41f0-9278-c1e356d5eb0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf7e5df2-365d-427a-af1b-ada3c3ac9add",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12b6a06b-f065-447e-b76d-ef1f72746fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf7e5df2-365d-427a-af1b-ada3c3ac9add",
                    "LayerId": "6ebf2f41-7dc3-400a-9559-b84f3182625f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6ebf2f41-7dc3-400a-9559-b84f3182625f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a936ed9f-4cb9-4fce-8704-0b6eed565176",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}