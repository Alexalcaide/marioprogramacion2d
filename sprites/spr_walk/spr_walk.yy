{
    "id": "dc836ce8-c6e2-4c9a-94a3-cb986ecee83f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8aaa0a00-269a-4213-94f8-edda9157cd82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc836ce8-c6e2-4c9a-94a3-cb986ecee83f",
            "compositeImage": {
                "id": "4bd1f0e6-e4dd-4e48-b9a0-425fbf8bf59b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aaa0a00-269a-4213-94f8-edda9157cd82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55d90dfd-bfa6-431b-a03f-894c98e968d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aaa0a00-269a-4213-94f8-edda9157cd82",
                    "LayerId": "312c3ad7-a676-4b0b-b2de-1041f8f843fe"
                }
            ]
        },
        {
            "id": "e6a96a32-eb0c-4a69-b6ff-7684eb9aa8f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc836ce8-c6e2-4c9a-94a3-cb986ecee83f",
            "compositeImage": {
                "id": "440f9219-2624-40f4-9517-94fdc0f4d1f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6a96a32-eb0c-4a69-b6ff-7684eb9aa8f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b5990ab-435c-4d9e-8ea3-6afc0ca7057a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6a96a32-eb0c-4a69-b6ff-7684eb9aa8f5",
                    "LayerId": "312c3ad7-a676-4b0b-b2de-1041f8f843fe"
                }
            ]
        },
        {
            "id": "f578ae87-cf4a-4692-838d-17a73bbef42c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc836ce8-c6e2-4c9a-94a3-cb986ecee83f",
            "compositeImage": {
                "id": "ac3e3a07-ce10-4e2f-812c-5002a1f665a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f578ae87-cf4a-4692-838d-17a73bbef42c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dba29018-a1a1-40f5-8ebd-1f604b5fdc14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f578ae87-cf4a-4692-838d-17a73bbef42c",
                    "LayerId": "312c3ad7-a676-4b0b-b2de-1041f8f843fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "312c3ad7-a676-4b0b-b2de-1041f8f843fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc836ce8-c6e2-4c9a-94a3-cb986ecee83f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 20
}