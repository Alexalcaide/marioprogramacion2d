{
    "id": "466ffb0a-21dc-4175-b238-543148619309",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be8843a9-f9da-4a99-8f18-130085b656c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "466ffb0a-21dc-4175-b238-543148619309",
            "compositeImage": {
                "id": "9cb2230d-3451-49b5-8c9b-b2602f3a81c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8843a9-f9da-4a99-8f18-130085b656c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8e61427-89aa-445e-97c3-ed32140c3d32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8843a9-f9da-4a99-8f18-130085b656c5",
                    "LayerId": "adc0e474-37c8-492b-b35e-4526d6f78245"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "adc0e474-37c8-492b-b35e-4526d6f78245",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "466ffb0a-21dc-4175-b238-543148619309",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 8
}