{
    "id": "51f497c3-a9f4-465f-ba13-1bf7b26e6e1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_destroyer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 29,
    "bbox_right": 30,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15238952-79ee-4940-8859-fd9415160f94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51f497c3-a9f4-465f-ba13-1bf7b26e6e1d",
            "compositeImage": {
                "id": "31b7299a-9670-420a-917e-8879c1ecdacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15238952-79ee-4940-8859-fd9415160f94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26291e99-c5b6-469f-b306-7bbc0a6d12de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15238952-79ee-4940-8859-fd9415160f94",
                    "LayerId": "a4c5261e-3f8d-495f-bc62-ad6d2687c8f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a4c5261e-3f8d-495f-bc62-ad6d2687c8f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51f497c3-a9f4-465f-ba13-1bf7b26e6e1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}