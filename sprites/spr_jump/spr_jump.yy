{
    "id": "1d52a486-be30-4486-9335-681351ad12a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c63e0f6-bb22-4dbe-b652-2a4579b5790a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d52a486-be30-4486-9335-681351ad12a8",
            "compositeImage": {
                "id": "fd6532ae-1dbc-4ee6-9d53-ff0ad2553bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c63e0f6-bb22-4dbe-b652-2a4579b5790a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e44cd96-1725-4a5b-b1e8-e2dcd2895870",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c63e0f6-bb22-4dbe-b652-2a4579b5790a",
                    "LayerId": "7203bae4-a8a0-4f4a-803d-80e08a2387ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "7203bae4-a8a0-4f4a-803d-80e08a2387ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d52a486-be30-4486-9335-681351ad12a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 21
}