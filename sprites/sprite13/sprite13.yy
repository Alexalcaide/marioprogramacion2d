{
    "id": "00a300c0-cc54-4f8a-879f-6abcf87f493d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 5,
    "bbox_right": 80,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db6a8449-e6ed-493e-8fac-094166cacb79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00a300c0-cc54-4f8a-879f-6abcf87f493d",
            "compositeImage": {
                "id": "ddb84e33-bcef-494d-80d6-3917bfb7295a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db6a8449-e6ed-493e-8fac-094166cacb79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc2da81-7eb9-4dc5-b8b5-8191433354f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db6a8449-e6ed-493e-8fac-094166cacb79",
                    "LayerId": "cceeb779-5165-4339-b8c7-42f96986c98a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "cceeb779-5165-4339-b8c7-42f96986c98a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00a300c0-cc54-4f8a-879f-6abcf87f493d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}