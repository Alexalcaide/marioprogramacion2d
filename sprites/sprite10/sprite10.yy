{
    "id": "6a15f82c-436c-4a4d-93dd-c1b0ac2675b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1a80612-e8a9-4f21-b048-4fe7b5604b21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a15f82c-436c-4a4d-93dd-c1b0ac2675b0",
            "compositeImage": {
                "id": "3fc17ece-28f9-4c5d-9052-a1c5337f32c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1a80612-e8a9-4f21-b048-4fe7b5604b21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea7fec7-ec4b-4137-967e-e29921012115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1a80612-e8a9-4f21-b048-4fe7b5604b21",
                    "LayerId": "92a13a1a-f6f8-4cb2-9743-ea0b760fb034"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "92a13a1a-f6f8-4cb2-9743-ea0b760fb034",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a15f82c-436c-4a4d-93dd-c1b0ac2675b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 0,
    "yorig": 0
}