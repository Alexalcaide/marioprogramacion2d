{
    "id": "f9f8fdaf-fd80-4ba8-8730-9dde71c1d260",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f47dd745-02cf-4d15-be8f-c27b775acca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "4939e598-2937-400b-bce2-bda4d768c86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 166,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5805adb0-7218-462f-bce5-cf98e26884df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3ad2d333-43c3-48fe-8684-6badff0450f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "04592d55-7781-4f2d-a9a6-edb24fde6991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3f8a76dc-ccd5-46f1-9f14-1cf2d6fbcdaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "cf18ac1c-c421-483d-b2ba-c2fa852ddea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "b27ff13e-291c-4a61-9ce6-fb08384a95d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "153f0562-e7dd-4936-86f9-0e584ca0acb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "01de1662-2350-4286-8da5-ab3962198f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9790f6a6-337d-4e66-9b8d-bd3ba8c32548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "217a8ee8-e3d3-4408-8682-c2d498cd492e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d0b2b042-ba75-4d90-8de8-a208c3fca893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fcaa9664-de30-417b-9e3f-8feb55370274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3f978d2a-fbea-4031-bf73-052e466ff6b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 156,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "809395a8-8ce4-4b6f-a110-ae4775b0e063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d5a2d902-b5f8-454c-849c-a84c8cee643a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 185,
                "y": 22
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8665cc6b-50db-479a-933a-bb22e2896beb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 86,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4313a1a5-fe04-4d55-85e9-3581b5bb8220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 196,
                "y": 22
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "80776c52-63f0-46fa-b541-a75b27696115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 207,
                "y": 22
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f17c43d0-e8c8-4714-956e-bb60e8ee1aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 22
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0c3df68f-fb88-40e2-a1e3-36a7c1c00dd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 22
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c518b5cf-4c31-4b49-9bec-5073d29dcafd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "095df8d7-688b-4759-afb9-a1ce83950445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d161b6c8-8fd1-4804-9217-29cda7e34fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "bc055654-7f25-4330-9cbd-df3dfcb9d761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "be61ba12-ee36-40c8-959c-5bb0424627de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 176,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d8260a51-aa8d-4755-8d20-ce7eb4d9ce08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 181,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "be3aa995-2c7f-488f-afc7-2fc9722f7874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "48cdd89b-8820-4915-8451-642c84bc0547",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2cbb797a-060b-4bee-b1a5-4331595dd642",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 174,
                "y": 22
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "83b5b9ab-5173-4e27-beba-c3aa76dfccb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 22
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1e384048-6ac4-4d0f-a171-c3c01d7ea7a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0a8eafac-6277-4683-b996-bc53e030c154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1a505232-12d9-43ba-9350-3588df7f8035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "11fb0537-3517-4fbc-8b0d-2a31dfe53016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5174a4d5-ae52-441d-be79-8fed5db4a79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ac1cac90-dd67-429c-bcfc-1baffe94a900",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 130,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "dfff44e3-9a57-4522-be71-673661eddbcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 22
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4763fb10-a289-4916-83af-85abad629564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6692a6e7-cb07-40fd-ba4a-9a2d2b1cfa9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b746c7cf-f469-4a35-84e2-b891185a8f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 171,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "56277023-4265-492a-b778-cebe937863fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "52e104d1-39c1-4657-954e-a018dc96f3d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "11a1e5e2-64ba-45b7-9c23-c2ec9616523b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4a5629ec-f6b1-4f1d-b766-03710d2bba44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "627a5df9-bc62-4871-bc75-b1039f0c5a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "567820f6-6136-4841-a6d4-d1ec02edf291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7302d573-262e-4c9d-8f72-5a821bd24329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 152,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "baccc602-fe59-4ee4-bdc5-6bc8238872ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c4ce3fbe-8bdb-4c36-b5c3-208598c637b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3e3efe80-e19d-484a-8c89-9b2536f2a061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d6a44137-b611-494e-862f-70cbfaea56c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8abc4e79-3b73-4abd-ae17-c4a4c46939c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 22
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7047f381-6473-4d56-a9b1-d993117572fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b55de384-b859-40d4-9aca-8041fe780cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0f550397-d5d8-481d-82bb-cfff2022624d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8929c330-8354-492d-8a96-1c09f16494a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c280964c-cb38-4e48-8200-6b2ca45b1fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 62,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cf8ef41f-c919-4463-a21b-417fff6193ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 145,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "22bb9cbe-e82d-4387-9ed0-83a317a238d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "22aba100-ddfc-4b50-a351-e4375b0cf3be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 127,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "63ce1e91-b60f-4596-b36d-f3ba6be7b220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "51f8b1ca-99b4-48d4-bb8b-3e438ec59a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b44f0f7e-3404-474a-9a29-d247f3a0ebb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 133,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1c98d0ab-d1c2-40f2-91b3-8dafee320f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 97,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "431a9edf-6024-40b6-b4b3-5408093258ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ecd8f043-c223-4eef-880b-81b65c9c3e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 151,
                "y": 42
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4dbc5f0c-1fd4-4a42-9edf-1280295e25c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "69392b05-12e2-4518-b5af-1c4192a330c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 108,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "59446d84-9aae-4c38-aee6-4b91d1e830f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "daf1fbe4-6c3a-4c2e-8531-70d37e6a7320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 161,
                "y": 42
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "6b1ab849-9200-4ed5-8d3a-4dd528d67155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 231,
                "y": 42
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fee232e4-5d20-42f2-93b6-1ac5b974b5b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 194,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "babbfd96-326c-4c50-87b7-ec91eab8ac99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "89354f2f-eb4b-4036-9998-fb898dbaf424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "dad1e58c-67f1-42e8-bb34-a33b41e59280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "055935a1-e220-4fa2-8b64-a9284e4eb234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "07f442a9-cfe4-4d5f-9b11-7f7fc8f46802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 240,
                "y": 42
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ef4d1fb1-ca3d-4690-bbbd-29e340f25a7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6b6f04c3-1308-43db-977c-6c3a4e98d216",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 141,
                "y": 42
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3981f178-268b-42c8-8250-9b0c77e0c83d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 42
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "eec872af-bf3f-4081-838d-06f56bdde297",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5478b76d-8002-4d1a-8c7b-00403015d201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6955e74d-c7c8-4fac-b1d7-ba1780b9c464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b8c15ee4-3d44-45d9-b5c2-d8b9c639848a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "11352d76-6298-48d5-8460-8254d8f81011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 42
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "cd021da6-457c-4d36-97c6-e53ab630ebfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "dffd4273-0e05-4b1a-9a3a-fb9b71ef35ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "654baa6e-bcba-4915-9c1c-bd760eb47192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "aa8f2d33-be80-49e5-84b1-eb2e7e1af087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 42
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a1e899fc-7e62-4ee9-bb90-ec7010a7b7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "570b27db-3059-4f5e-a218-386a061a25da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 190,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5e246e22-9ec6-4eab-9e43-0195b027c3e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "940de4ab-b173-42a7-912e-20d0a41e7919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 163,
                "y": 22
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d0b2efd4-d00e-42e6-af7f-6ba23303c372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "aaae1489-bbc6-484c-86d2-bd2ef7e84db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "49a4e2f1-ab7b-4c5b-81e4-706c75302806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "22cfe6d0-2850-439d-85f9-a08c383cc2e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "deaa4114-7ac9-41b7-a516-d4cab42a01c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "595ffa42-8cb6-4d71-9ef7-92590d0dd897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "9c80fb7d-56dc-4e80-805e-588cec976ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "498abd93-5afc-4969-86e1-976889257609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "56341681-447f-4649-963a-cfdbe0f6ab54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "74a5712c-6fe2-4c74-ac49-52e9c14d2bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "61dfb8d1-b611-4e39-a67d-0b892cf6458e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "cff7a330-383d-46a3-b295-a6d541bf64e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "8d6eb7a3-c65d-48d7-a485-e0dbb8e4d3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "77f59466-ffbc-4a34-9437-820a7df197f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "777997b5-fc97-4779-aa63-f6796e492513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "10383aef-5850-43ce-b3cb-646c88792bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "5cc86c5a-257c-4073-957b-cf423fd599b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "54d08abd-6bdc-4f48-87ec-9d6b364635d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "1f5d3638-2e11-4371-97e0-1a899cb64a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "1f6191e1-09e7-4bd5-91bd-1b140956078a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "64c425b1-11b4-4723-a4a4-f0b559913b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "349f14c1-f04d-4323-b225-c29483ed829b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "1ef22292-73d0-4d10-a9f2-745f34b67772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "ef9de607-345c-47fb-9a1d-5370b9b18c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "1063bdef-b9ce-47eb-b0cb-0a8dd15e7176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "6a53783d-dc99-40b4-ba5b-321b7944f58f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "2c5c8b8e-1690-44ec-8e57-c8fc388139b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "8cefdb2e-e620-47e2-8001-92401189afdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "93df54bf-717f-48dc-a248-75b6f6c3284b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "92b816cb-6a9e-462b-b500-11246c416e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "88b25b23-ffda-420c-8d98-eba02f559a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "b4691048-3453-43dc-b297-750e300a5a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "444b90b6-f375-45dd-adaf-9e196c3a5795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "90d9e136-50f8-473e-a430-045bdccaf4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "2dcbf0f7-8d04-453e-ae62-2a6a79c0b3cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "758d3cb1-47d8-4b6e-ad97-1dff1e7284c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "9e9a051f-6326-4293-a995-dd1843e4c63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "b34350cd-48e3-4603-bcea-4085190820ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "fdccee19-b773-4ebf-918c-ed538bf9873b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "218c8bb0-b8b8-4a27-ae63-fffb3e24b0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "3e460224-8591-4d3f-adc3-d5a0be15253b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "203df2d3-b32c-423c-84f4-05441fb814c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "f6a9ea25-81b7-4b16-9752-a3cdcd4e3f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "5cbfe68e-a3a2-45e1-8cd7-5c69b02436ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "a7b0f7b6-f394-42c1-86bc-4f8512545034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "535c6a7b-d6d3-478b-929d-a056e87dd833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "2c1b8492-988f-486f-b1fa-f0da75b8a0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "b3f0b87a-d164-406a-931c-b24bce7f1861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "5641c379-2c7c-4130-b4dd-8b11913cc971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "72135530-8eb9-4566-ac27-5d9f9be92992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "857f6107-80a9-4bc5-8c57-c88a70dd7837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "d5b4776f-9c5c-4806-8ab9-d21ede9ed220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "d4a87391-25ec-4ebc-a111-1e0e096fb9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "ff39acb4-dd96-4621-bd7e-d071308374eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "2b7d7c7d-1509-4622-803a-2da4d17ed595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "942b9f0f-ea05-4462-ae59-24ccc4f9d9e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "5314a895-5315-4705-b432-448648d507c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "53dbec89-ee1c-4cd9-975d-f62a1d24fd62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "50053777-f61f-48e2-8413-6914ae74ebc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "25132d10-9f69-4400-8013-a533d0fef1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "f1226bba-0aa9-47f0-89db-f30e758df163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "8aecefad-bae4-413d-8780-8b1d01b548a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "9a29e6a9-d5c4-4f64-b14f-ad6a654a995c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "3fb624f9-5423-441d-8705-82cec7e1770c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "5919988f-83c9-4641-9d3f-618d2e38287e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "3ab7d5bb-330f-4d8d-8509-fe423525728b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "585c415d-77f1-4d02-9ce0-bf3f4d578469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "33f3c799-cc84-4501-ac24-330ac7a94197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "fbe5b42c-7856-4f56-8292-58c32b6de19f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "4394df6f-bdeb-4ff3-841d-4666870f0724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "0076fe43-5e15-4d03-affe-908f0d797031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}